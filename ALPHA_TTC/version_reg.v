module version_reg (data_out);
    output [31:0] data_out;
    reg [31:0] data_out;
    always @ (1) begin
            data_out <= 32'h7F120921;
    end
endmodule
