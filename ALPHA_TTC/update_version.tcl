# Creates a register bank in a verilog file with the specified hex value
proc generate_version { hex_value } {

    set num_digits [string length $hex_value]
    set bit_width [expr { 4 * $num_digits } ]
    set high_index [expr { $bit_width - 1 } ]
    set reset_value [string repeat "0" $num_digits]

    if { [catch {
        set fh [open "version_reg.v" w ]
        puts $fh "module version_reg (data_out);"
        puts $fh "    output \[$high_index:0\] data_out;"
        puts $fh "    reg \[$high_index:0\] data_out;"
        puts $fh "    always @ (1) begin"
        puts $fh "            data_out <= ${bit_width}'h${hex_value};"
        puts $fh "    end"
        puts $fh "endmodule"
        close $fh
    } res ] } {
        return -code error $res
    } else {
        return 1
    }
}

# This line accommodates script automation
#foreach { flow project revision } $quartus(args) { break }

set str [clock format [clock seconds] -format {7F%y%m%d}]
#set out [format "%X" $str]

#set revision_number "aaaaaa"
set revision_number "$str";

# Call procedure to store the number
if { [catch { generate_version $revision_number } res] } {
    post_message -type critical_warning "Couldn't generate Verilog file. $res"
} else {
    post_message "Successfully updated version number to version 0x${revision_number}"
}

proc generate_timestamp { value } {
    if { [catch {
        set fh [open "timestamp.v" w ]
        puts $fh "module timestamp (data_out);"
        puts $fh "    output \[31:0\] data_out;"
        puts $fh "    reg \[31:0\] data_out;"
        puts $fh "    always @ (1) begin"
        puts $fh "       data_out <= 32'h${value};"
        puts $fh "    end"
        puts $fh "endmodule"
        close $fh
    } res ] } {
        return -code error $res
    } else {
        return 1
    }
}

foreach { flow project revision } $quartus(args) { break }

#set str [clock format [clock seconds] -format {%y%m%d}]
#set revision [expr [clock seconds] >> 16]
set revision [format %x [clock seconds]]

if { [catch { generate_timestamp $revision } res] } {
    post_message -type critical_warning "Couldn't generate Timestamp file. $res"
} else {
    post_message "updated timestamp to [clock format [expr 0x$revision]]"
}

#end
