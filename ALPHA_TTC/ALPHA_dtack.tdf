TITLE "VME synchronization & DTACK generation";
include "lpm_counter.inc";

SUBDESIGN ALPHA_dtack
(
	sysclk 			: INPUT;
	module_selN		: INPUT;
	
	vme_asN			: INPUT;
	vme_ds0N		: INPUT;
	vme_ds1N		: INPUT;
	vme_wrN			: INPUT;
	vme_add[3..1]	: INPUT;	%add[3..1] == request level during iack cycle %
	vme_iackinN		: INPUT;
	vme_iackN		: INPUT;
	irq_level[2..0]	: INPUT;
	IntRequActive	: INPUT;
	resetN			: INPUT;

	245buff_oeN		: OUTPUT;
	Sync_WrtStrb	: OUTPUT;
	wr_strbN		: OUTPUT;
	vme_data_oe		: OUTPUT;
	vector_oeN		: OUTPUT;	
	iack_outN		: OUTPUT;	% pass on iack if not this module %
	Clr_IrqN		: OUTPUT;	
	asN				: OUTPUT;
	
	Mem_acc_clk		: OUTPUT;
	vme_accN		: OUTPUT;
	dtack			: OUTPUT;
	dtack_done		: OUTPUT;
	
	VME_BusyN		: OUTPUT;
	
	state_idle      : OUTPUT;
	state_sram_wrt_setup1: OUTPUT;
	state_sram_wrt_setup2: OUTPUT;
	state_data_setup: OUTPUT;
	state_data_acc: OUTPUT;
	state_st_dtack: OUTPUT;
	state_end_dtack: OUTPUT;
	
	vme_start: OUTPUT;
)

VARIABLE

	wrt_strb_ff		:DFFE;
	Sync_WrtStrb_ff	:DFFE;
	Mem_acc_clk_ff	:DFFE;
	245buff_oeN_ff	:DFF;
	vme_data_oe_ff	:DFF;
	vme_accN_ff		:DFF;
	%vme_dtack_ff	:DFF;%
	vector_oe_ff	:SRFF;
	iack_cycleN		:NODE;
	
	vme_dtack:	MACHINE 

					WITH STATES (
				 		idle  = B"0001",
						--- idle2 = B"0010",
						sram_wrt_setup1 = B"0100",
						sram_wrt_setup2 = B"0101",
						data_setup = B"0110", 
				 		data_acc   = B"0111",
				 		st_dtack   = B"1000",
						end_dtack  = B"1001"
						);
--				 		chk_plevel,
--				 		setup_vector,
--				 		setup_vector1,
--				 		wait_rem_vec,
--				 		pass_iack
--				);
				
			
BEGIN
 	DEFAULTS
		wrt_strb_ff.d		= VCC;	% device write inactive %
		Sync_WrtStrb_ff.d	= GND;
		Mem_acc_clk_ff.d	= GND;
		vme_data_oe_ff.d	= GND;
		iack_outN			= VCC;
		%vme_dtack_ff.d		= GND;%
		asN					= VCC;
		
		state_idle = GND;
		state_sram_wrt_setup1 = GND;
		state_sram_wrt_setup2 = GND;
		state_data_setup = GND;
		state_data_acc = GND;
		state_st_dtack = GND;
		state_end_dtack = GND;
		
  	END DEFAULTS;

	% Device access clock  %
	Mem_acc_clk_ff.clk	= sysclk;
	Mem_acc_clk_ff.d	= (sram_wrt_setup2 or data_acc);
	Mem_acc_clk_ff.clrn	= !(vme_ds1N and vme_ds0N);
	Mem_acc_clk 		= Mem_acc_clk_ff.q;
	
	Sync_WrtStrb_ff.clk		= sysclk;
	Sync_WrtStrb_ff.d		= (data_acc);
	Sync_WrtStrb_ff.clrn	= !(vme_ds1N and vme_ds0N);
	Sync_WrtStrb	 		= Sync_WrtStrb_ff.q;
	

	% vme internal data output enable %
	vme_data_oe_ff.clk	= sysclk;
	vme_data_oe_ff.d	= !idle and vme_wrN and !module_selN;
	vme_data_oe_ff.clrn	= !(vme_ds1N and vme_ds0N);
	vme_data_oe 		= vme_data_oe_ff.q;
	
	245buff_oeN_ff.clk	= sysclk;
	245buff_oeN_ff.d	= module_selN;
	245buff_oeN_ff.prn	= !(vme_ds1N and vme_ds0N);
--  !245buff_oeN 		= !245buff_oeN_ff.q or !vector_oe_ff.q;
	!245buff_oeN 		= !245buff_oeN_ff.q;

	% internal register write strobe %
	wrt_strb_ff.clk	= 	 !sysclk;
	wrt_strb_ff.prn =	!(vme_ds1N and vme_ds0N);
	wr_strbN		=	wrt_strb_ff.q;
	
	vme_start = !module_selN and (!vme_ds0N or !vme_ds1N) and !vme_asN and vme_iackN and resetN;
	
	% vme data cycle %	
	vme_accN_ff.clk = sysclk;
	vme_accN_ff.d	= vme_start;
	!vme_accN		= vme_accN_ff.q;

	% dtack output 
	vme_dtack_ff.clk	= sysclk;
	vme_dtack_ff.d		= st_dtack or wait_rem_vec;
	vme_dtack_ff.clrn	= !(vme_ds1N and vme_ds0N);%
--	dtack				= st_dtack or wait_rem_vec;
    dtack = st_dtack;
	dtack_done			= end_dtack;

	% interrupt acknowledge cycle %
	!iack_cycleN =		!vme_iackinN & !vme_iackN & !vme_asN & (!vme_ds0N # !vme_ds1N) & resetN;
--	!Clr_IrqN	 =		 wait_rem_vec;
		
	% vme vector output enable %
--	vector_oe_ff.clk	= sysclk;
--	vector_oe_ff.r		= setup_vector;
--	vector_oe_ff.s		= idle;
--	vector_oe_ff.prn	= !(vme_ds1N and vme_ds0N);
--	vector_oeN			= vector_oe_ff.q;	
	
	%VME_BusyN = idle;%
	VME_BusyN = !data_setup;
	
	% state machine assignments %
	vme_dtack.clk	= sysclk;
	vme_dtack.reset	= !resetN;

CASE	vme_dtack IS

	WHEN	idle	=>
			state_idle = VCC;
			IF 		!vme_accN THEN						% respond to a data cycle %
--					 wrt_strb_ff.d		= vme_wrN;		% generate write strobes if a write cycle %			
					 vme_dtack = sram_wrt_setup1;
				
--			ELSIF	!iack_cycleN AND IntRequActive THEN % respond to iack cycle %
--					 vme_dtack = chk_plevel;
			ELSE
					 vme_dtack = idle;
			END IF;
			
	---WHEN    idle2  =>
	---		vme_dtack = idle;

			% data access %
	WHEN	sram_wrt_setup1 =>
			state_sram_wrt_setup1 = VCC;
			asN = GND;
			wrt_strb_ff.d		= vme_wrN;		% generate write strobes if a write cycle %
			vme_dtack =	sram_wrt_setup2;
				
	WHEN 	sram_wrt_setup2 =>
			state_sram_wrt_setup2 = VCC;
			wrt_strb_ff.d		= vme_wrN;		% generate write strobes if a write cycle %
			vme_dtack 			= data_setup;
				
	WHEN	data_setup =>
			state_data_setup = VCC;
			wrt_strb_ff.d		= vme_wrN;		% generate write strobes if a write cycle %
			vme_dtack =	data_acc;
				
	WHEN	data_acc =>
			state_data_acc = VCC;
			vme_dtack =	st_dtack;
															
			% data acknowledge cycle %
	WHEN	st_dtack	=>
			state_st_dtack = VCC;

			--IF	!vme_ds0N # !vme_ds1N THEN
			IF	!vme_ds0N or !vme_ds1N THEN
				vme_dtack =	st_dtack;			% wait for master to remove DS0, DS1 %
			ELSE
				vme_dtack 	= end_dtack;	
			END IF;	
			
	WHEN	end_dtack	=>
			state_end_dtack = VCC;
			vme_dtack 	= idle;	

--		% interrupt acknowledge cycle %
--	WHEN 	chk_plevel =>
--			IF		!vme_iackinN
--				 & 	(vme_add[3..1] == irq_level[2..0])	% do priority levels match?%
--			THEN
--				vme_dtack =	setup_vector;						
--			ELSE
--				vme_dtack 	= pass_iack;				% pass iackinN to next slot %
--			END IF;
--
--	WHEN	setup_vector =>
--			vme_dtack =	setup_vector1;
--
--	WHEN	setup_vector1 =>
--			vme_dtack =	wait_rem_vec;				% wait for master to remove iackinN %
--
--	WHEN	wait_rem_vec =>
--			IF !vme_ds0N # !vme_ds1N THEN
--				vme_dtack 	= wait_rem_vec;
--			ELSE
--				vme_dtack 	= idle;	
--			END IF;
--
--	WHEN	pass_iack =>
--			IF !vme_ds0N # !vme_ds1N THEN
--				iack_outN = vme_asN;					% pass iack to next module %
--				vme_dtack = pass_iack;					% wait for iack cycle to end %
--			ELSE
--				dtack			= GND;					% remove dtack %
--				vme_dtack 	= idle;	
--			END IF;	
	WHEN OTHERS	=>
			vme_dtack 	= idle;	
END CASE;

END;

