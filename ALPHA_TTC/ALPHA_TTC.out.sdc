## Generated SDC file "ALPHA_TTC.out.sdc"

## Copyright (C) 1991-2007 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 7.2 Build 203 02/05/2008 Service Pack 2 SJ Full Version"

## DATE    "Thu Jun  5 17:37:05 2008"

##
## DEVICE  "EP1C6Q240C6"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {50MHz_Osc} -period 20.000 -waveform { 0.000 10.000 } [get_ports {50MHz_Osc}] -add
create_clock -name {Trig_Logic_Clk} -period 50.000 -waveform { 0.000 25.000 } [get_ports {Trig_Logic_Clk}] -add
create_clock -name {PLL_20MHz} -period 50.000 -waveform { 0.000 25.000 } [get_ports {PLL_20MHz}] -add


#**************************************************************
# Create Generated Clock
#**************************************************************

derive_pll_clocks -use_tan_name


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -exclusive -group [get_clocks {  Trig_Logic_Clk  }] -group [get_clocks {  altpll0:inst5|altpll:altpll_component|_clk0  altpll0:inst5|altpll:altpll_component|_clk1  altpll0:inst5|altpll:altpll_component|_extclk0  50MHz_Osc  }] -group [get_clocks {  PLL_20MHz  }] 


#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************



#**************************************************************
# Set Load
#**************************************************************

