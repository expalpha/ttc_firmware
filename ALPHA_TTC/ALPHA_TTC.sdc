## Generated SDC file "ALPHA_TTC.sdc"

## Copyright (C) 1991-2011 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 11.1 Build 216 11/23/2011 Service Pack 1 SJ Full Version"

## DATE    "Fri Feb 24 17:23:54 2012"

##
## DEVICE  "EP1C6Q240C6"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {50MHz_Osc} -period 20.000 -waveform { 0.000 10.000 } [get_ports {50MHz_Osc}]
create_clock -name {PLL_20MHz} -period 50.000 -waveform { 0.000 25.000 } [get_ports {PLL_20MHz}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {altpll0:PLL2|altpll:altpll_component|_clk0} -source [get_pins {PLL2|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 1 -master_clock {50MHz_Osc} [get_pins {PLL2|altpll_component|pll|clk[0]}] 
create_generated_clock -name {altpll0:PLL2|altpll:altpll_component|_clk1} -source [get_pins {PLL2|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 2 -divide_by 5 -master_clock {50MHz_Osc} [get_pins {PLL2|altpll_component|pll|clk[1]}] 
create_generated_clock -name {altpll0:PLL2|altpll:altpll_component|_extclk0} -source [get_pins {PLL2|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 2 -divide_by 5 -master_clock {50MHz_Osc} [get_pins {PLL2|altpll_component|pll|extclk[0]}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -exclusive -group [get_clocks {  Trig_Logic_Clk  }] -group [get_clocks {  altpll0:inst5|altpll:altpll_component|_clk0  altpll0:inst5|altpll:altpll_component|_clk1  altpll0:inst5|altpll:altpll_component|_extclk0  50MHz_Osc  }] -group [get_clocks {  PLL_20MHz  }] 


#**************************************************************
# Set False Path
#**************************************************************

set_false_path -to [get_keepers {*signaltap*}]
set_false_path -from [get_keepers {regACr*}] -to [get_keepers {*}]
set_false_path -from [get_keepers {regC0r*}] -to [get_keepers {*}]
set_false_path -from [get_keepers {regB0r*}] -to [get_keepers {*}]
set_false_path -from [get_keepers {regBCr*}] -to [get_keepers {BusyDelay*}]
set_false_path -from [get_keepers {regC0r*}] -to [get_keepers {AdcDelay*}]
set_false_path -from [get_keepers {regC0r*}] -to [get_keepers {HoldDelay*}]
set_false_path -from [get_keepers {regC0r*}] -to [get_keepers {VetoDelay*}]
set_false_path -from [get_keepers {regC0r*}] -to [get_keepers {*}]
set_false_path -from [get_keepers {regCCr*}] -to [get_keepers {*}]
set_false_path -from [get_keepers {*}] -to [get_keepers {cal_pulse_sync}]
set_false_path -from [get_keepers {*}] -to [get_keepers {alpha_busy_extend_DFF}]
set_false_path -from [get_keepers {cmd_trigger_dff}] -to [get_keepers {trigger_sync}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

